package com.koncert.doris;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.koncert.doris.databinding.ActivityYoutubeBinding;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerListener;

public class YoutubeActivity extends AppCompatActivity {
    private ActivityYoutubeBinding binding;
    private final String youtubeLink = "L5No4rPh2L0";
    public float videoDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityYoutubeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.youtubeView.initialize(
                initializedYouTubePlayer -> initializedYouTubePlayer.addListener(new YouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        initializedYouTubePlayer.loadVideo(youtubeLink, 0);
                    }

                    @Override
                    public void onStateChange(@NonNull PlayerConstants.PlayerState state) {

                    }

                    @Override
                    public void onPlaybackQualityChange(@NonNull PlayerConstants.PlaybackQuality playbackQuality) {

                    }

                    @Override
                    public void onPlaybackRateChange(@NonNull PlayerConstants.PlaybackRate playbackRate) {

                    }

                    @Override
                    public void onError(@NonNull PlayerConstants.PlayerError error) {

                    }

                    @Override
                    public void onApiChange() {

                    }

                    @Override
                    public void onCurrentSecond(float second) {
                        if (second >= videoDuration) {
                            binding.youtubeView.release();
                            YoutubeActivity.this.finish();
                        }
                    }

                    @Override
                    public void onVideoDuration(float duration) {
                        videoDuration = duration;
                    }

                    @Override
                    public void onVideoLoadedFraction(float loadedFraction) {

                    }

                    @Override
                    public void onVideoId(@NonNull String videoId) {

                    }
                }), true);

    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.youtubeView.release();
    }
}
