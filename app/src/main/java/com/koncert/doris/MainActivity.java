package com.koncert.doris;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.koncert.doris.databinding.ActivityMainBinding;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private Timer timer = new Timer();
    private final String concertDateTime = "2021-11-30 20:30:00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.imageSlider.setSliderAdapter(new SliderAdapterExample());
        binding.imageSlider.setIndicatorAnimation(IndicatorAnimations.SCALE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        binding.imageSlider.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        binding.imageSlider.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
        binding.imageSlider.setIndicatorSelectedColor(Color.WHITE);
        binding.imageSlider.setIndicatorUnselectedColor(Color.GRAY);
        binding.imageSlider.setScrollTimeInSec(4); //set scroll delay in seconds :
        binding.imageSlider.startAutoCycle();

        binding.btnBuy.setOnClickListener(v -> openPage());
        binding.fabPlay.setOnClickListener(view1 -> startActivity(new Intent(MainActivity.this, YoutubeActivity.class)));

    }

    public void getDateDifference(String FutureDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.UK);

        try {
            Date currentDate = dateFormat.parse(FutureDate);
            System.out.println(currentDate);

            Date oldDate = new Date();

            long diff = currentDate.getTime() - oldDate.getTime();

            long days = diff / (24 * 60 * 60 * 1000);
            diff -= days * (24 * 60 * 60 * 1000);

            long hours = diff / (60 * 60 * 1000);
            diff -= hours * (60 * 60 * 1000);

            long minutes = diff / (60 * 1000);
            diff -= minutes * (60 * 1000);

            long seconds = diff / 1000;

            System.out.println("Dani:" + (days + ""));
            System.out.println("Minute:" + (minutes + ""));
            System.out.println("Sati:" + hours + "");
            System.out.println("Sekunde:" + seconds + "");

            String styledText = "<font color='red'>" + days + "</font>" + " dana.<br>";
            String styledText2 = "<font color='red'>" + hours + "</font>" + " sati.<br>";
            String styledText3 = "<font color='red'>" + minutes + "</font>" + " minuta.<br>";
            String styledText4 = "<font color='red'>" + seconds + "</font>" + " sekundi.<br>";

            binding.lblCalculation.setText(
                    Html.fromHtml(styledText
                            + System.lineSeparator() + styledText2
                            + styledText3
                            + styledText4), TextView.BufferType.SPANNABLE);

            binding.progress.setVisibility(View.GONE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(() -> getDateDifference(concertDateTime));
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        binding.progress.setVisibility(View.GONE);
        binding.lblCalculation.setText("");
        timer.cancel();
        timer.purge();
        timer = null;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        binding.progress.setVisibility(View.VISIBLE);

        if (timer == null) {
            timer = new Timer();
        }

        timer.schedule(new MyTimerTask(), 1000, 1000);

    }

    private void openPage() {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse("https://www.eventim.hr/hr/izvodjac/doris-dragovic-1634/profile.html"));
        startActivity(i);
    }
}
